fun main() {
    retainElementsExample()
}

fun removeElementsExample() {
    val list: MutableCollection<Int> = LinkedList()
    list.add(3)
    list.add(2)
    list.add(1)

    println(list)
    list.remove(1)
    println(list)
}

fun removeAllElementsExample(){
    val list: MutableCollection<Int> = LinkedList()
    list.add(3)
    list.add(2)
    list.add(1)
    list.add(4)
    list.add(5)

    println(list)
    list.removeAll(listOf(3,4,5))
    println(list)

}

fun retainElementsExample(){
    val list: MutableCollection<Int> = LinkedList()
    list.add(3)
    list.add(2)
    list.add(1)
    list.add(4)
    list.add(5)

    println(list)
    list.retainAll(listOf(3,4,5))
    println(list)

}

fun printDoublesExamples() {
    val list = LinkedList<Int>()
    list.push(3).push(2).push(1)

    println(list)

    for (item in list) {
        println("Double item: ${item * 2}")
    }
}

fun nodeExample() {
    val node1 = Node(value = 1)
    val node2 = Node(value = 2)
    val node3 = Node(value = 3)

    node1.next = node2
    node2.next = node3

    print(node1)
}

fun pushExample() {
    val list = LinkedList<Int>()
    list.push(3)
    list.push(2)
    list.push(1)

    println(list)
}

fun fluentInterfacePush() {
    val list = LinkedList<Int>()
    list.push(3).push(2).push(1)
    println(list)
}

fun appendExample() {
    val list = LinkedList<Int>()
    list.append(1).append(2).append(3)
    println(list)
}

fun insertingAtAParticularIndexExample() {
    val list = LinkedList<Int>()
    list.push(3).push(2).push(1)

    println("Before inserting: $list")

    var middleNode = list.nodeAt(1)

    for (i in 1..3) {
        middleNode = middleNode?.let { list.insert(-1 * i, it) }

        println("After inserting: $list")
    }
}

fun popExample() {
    val list = LinkedList<Int>()
    list.push(3).push(2).push(1)

    println("Before popping list: $list")
    val poppedValue = list.pop()
    println("After popping list: $list")
    println("Popped value: $poppedValue")

}

fun removingTheLastNodeExample() {
    val list = LinkedList<Int>()
    list.push(3).push(2).push(1)

    println("Before removing at node: $list")
    val removedValue = list.removeLast()
    println("After removing list: $list")
    println("Removed value: $removedValue")

}

fun removingANodeAfterAParticularNodeExample() {
    val list = LinkedList<Int>()
    list.push(3).push(2).push(1)

    val index = 1
    val node = list.nodeAt(index - 1)
    val removedValue = node?.let { list.removeAfter(it) }

    println("After removing at index $index: $list")
    println("Removed value: $removedValue")
}